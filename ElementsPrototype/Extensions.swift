//
//  Extensions.swift
//  ElementsPrototype
//
//  Created by Vasilis Akoinoglou on 23/11/2018.
//  Copyright © 2018 Vasilis Akoinoglou. All rights reserved.
//

import Foundation
import SceneKit

/*----------------------------------------------------------------------------*/

func round(val: SCNVector3) -> SCNVector3 {
    return SCNVector3(x: round(val.x), y: round(val.y), z: round(val.z))
}


func + (left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    return SCNVector3(x: left.x + right.x, y: left.y + right.y, z: left.z + right.z)
}

func - (left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    return SCNVector3(x: left.x - right.x, y: left.y - right.y, z: left.z - right.z)
}

func * (left: SCNVector3, right: SCNVector3) -> CGFloat {
    return CGFloat(left.x * right.x + left.y * right.y + left.z * right.z)
}


func * (left: SCNVector3, right: CGFloat) -> SCNVector3 {
    return SCNVector3(x: left.x * right, y: left.y * right, z: left.z * right)
}

func * (left: CGFloat, right: SCNVector3) -> SCNVector3 {
    return SCNVector3(x: left * right.x, y: left * right.y, z: left * right.z)
}

/*----------------------------------------------------------------------------*/

class DraggableSceneView: SCNView {

    var mark : SCNNode? = nil
    var selection : SCNHitTestResult? = nil
    var hitOld = SCNVector3Zero

    // mark an object (= selection)
    override func mouseDown(with event: NSEvent) {

        let p = convert(event.locationInWindow, from: nil)
        let options = [
            SCNHitTestOption.sortResults : true,
            SCNHitTestOption.boundingBoxOnly : true
        ]

        let hitResults = hitTest(p, options: options)

        if (hitResults.count > 0){
            let result: AnyObject = hitResults[0]
            if  result is SCNHitTestResult {
                selection = result as? SCNHitTestResult
            }
        }

        super.mouseDown(with: event)
    }


    // if there is a marked object, clone it and move it
    override func mouseDragged(with event: NSEvent) {

        if selection != nil {
            let mouse   = convert(event.locationInWindow, from: self)
            var unPoint = unprojectPoint(SCNVector3(x: mouse.x, y: mouse.y, z: 0.0))
            let p1      = selection!.node.parent!.convertPosition(unPoint, from: nil)
            unPoint     = unprojectPoint(SCNVector3(x: mouse.x, y: mouse.y, z: 1.0))
            let p2      = selection!.node.parent!.convertPosition(unPoint, from: nil)
            let m       = p2 - p1

            let e       = selection!.localCoordinates
            let n       = selection!.localNormal

            let t       = ((e * n) - (p1 * n)) / (m * n)
            let hit     = SCNVector3(x: p1.x + t * m.x, y: p1.y + t * m.y, z: p1.z + t * m.z)
            let offset  = hit - hitOld
            hitOld      = hit

            if mark != nil {
                mark!.position = mark!.position + offset

            } else {
                mark = selection!.node.clone()
                mark!.opacity = 0.333
                mark!.position = selection!.node.position
                selection!.node.parent!.addChildNode(mark!)
            }

        } else {
            super.mouseDragged(with: event)
        }
    }


    //   when the mouse button is released
    // + an object was marked
    // + the CRTL button is pressed
    // = copy the object (means: don't remove the cloned object)
    override func mouseUp(with event: NSEvent) {

        if selection != nil {

            if event.modifierFlags == NSEvent.ModifierFlags.control {
                mark!.opacity = 1.0

            } else {
                selection!.node.position = selection!.node.convertPosition(mark!.position, from: selection!.node)
                mark!.removeFromParentNode()
            }

            selection = nil
            mark = nil

        } else {
            super.mouseUp(with: event)
        }
    }
}
