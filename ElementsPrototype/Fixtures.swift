//
//  Fixtures.swift
//  ElementsPrototype
//
//  Created by Vasilis Akoinoglou on 20/11/2018.
//  Copyright © 2018 Vasilis Akoinoglou. All rights reserved.
//

import Cocoa
import SceneKit

struct DemoPreset {

    static var rustedIron: SCNMaterial = {
        let material = SCNMaterial() 
        material.lightingModel      = .physicallyBased
        material.diffuse.contents   = NSImage(named: "rustediron-streaks-albedo")
        material.roughness.contents = NSImage(named: "rustediron-streaks-roughness")
        material.metalness.contents = NSImage(named: "rustediron-streaks-metal")
        material.normal.contents    = NSImage(named: "rustediron-streaks-normal")
        return material
    }()

}
