//
//  MaterialController.swift
//  ElementsPrototype
//
//  Created by Vasilis Akoinoglou on 20/11/2018.
//  Copyright © 2018 Vasilis Akoinoglou. All rights reserved.
//

import Cocoa
import SceneKit

class MaterialController: NSViewController {

    @objc dynamic var materialContentModes: [String] = ["Color", "Image", "Value"]

    @objc dynamic var material: SCNMaterial?

    override var representedObject: Any? {
        didSet {
            material = representedObject as? SCNMaterial
        }
    }



}
