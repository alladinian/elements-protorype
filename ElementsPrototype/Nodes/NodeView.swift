//
//  NodeView.swift
//  NodeFlow
//
//  Created by Vasilis Akoinoglou on 30/11/2018.
//

import Cocoa

@objc protocol ControllableProperty: NSObjectProtocol {
    var name: String { get set }
    var value: CGFloat { get set }
}

protocol NodeIO {
    var name: String { get }
    var xinputs: [ControllableProperty] { get }
    var xoutputs: [ControllableProperty] { get }
}

public class ConnectionView: NSView {

    let ring = CALayer()
    let circle = CALayer()

    var isConnected: Bool = false
    var isHighlighted: Bool = false
    var isInput: Bool!

    convenience init() {
        self.init(frame: NSRect(x: 0, y: 0, width: 16, height: 16))
    }

    public override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        commonInit()
    }

    public required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        commonInit()
    }

    func commonInit() {
        wantsLayer = true

        ring.frame = bounds
        ring.cornerRadius = ring.bounds.midY
        ring.borderColor = NSColor.controlAccentColor.cgColor
        ring.borderWidth = 2
        layer?.addSublayer(ring)

        circle.frame           = bounds.insetBy(dx: 4, dy: 4)
        circle.backgroundColor = NSColor.controlAccentColor.cgColor
        circle.cornerRadius    = circle.bounds.midY
        layer?.addSublayer(circle)

        widthAnchor.constraint(equalToConstant: bounds.width).isActive = true
        heightAnchor.constraint(equalToConstant: bounds.height).isActive = true

        circle.opacity = 0.0
    }

    override public func updateTrackingAreas() {
        for trackingArea in self.trackingAreas {
            self.removeTrackingArea(trackingArea)
        }
        let options: NSTrackingArea.Options = [.mouseEnteredAndExited, .activeAlways]
        let trackingArea = NSTrackingArea(rect: self.bounds, options: options, owner: self, userInfo: nil)
        self.addTrackingArea(trackingArea)
    }

    public override func mouseEntered(with event: NSEvent) {
        isHighlighted = true
        circle.opacity = isConnected ? 1.0 : 0.5
    }

    public override func mouseExited(with event: NSEvent) {
        isHighlighted = false
        circle.opacity = isConnected ? 1.0 : 0.0
    }
}

public class NodeView: NSView {

    private(set) var isSelected: Bool = false {
        didSet {
            needsDisplay = true
        }
    }

    var representedObject: NodeIO!

    init(object: NodeIO) {
        super.init(frame: .zero)
        representedObject = object
        commonInit()
    }

    public override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        commonInit()
    }

    public required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        commonInit()
    }

    func commonInit() {
        translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false

        wantsLayer = true
        layer?.cornerRadius = kCornerRadius
        layer?.borderWidth = 2

        addSubview(titleLabel)
        titleLabel.alignment = .center
        titleLabel.font = NSFont.boldSystemFont(ofSize: 13)
        titleLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        titleLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
        titleLabel.setContentCompressionResistancePriority(.required, for: .vertical)

        addSubview(stackView)
        stackView.orientation = .vertical
        stackView.alignment   = .leading
        stackView.distribution = .fill
        stackView.spacing     = 8
        stackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        stackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        stackView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        stackView.heightAnchor.constraint(greaterThanOrEqualToConstant: 24).isActive = true

        setupHeader()
        setupShadow()
        setupOutputs()
        setupInputs()

        needsDisplay = true
    }

    public var title: String! {
        didSet {
            titleLabel.stringValue = title
        }
    }

    public var color: NSColor! {
        didSet {
            layer?.backgroundColor = color.cgColor
        }
    }

    public var headerColor: NSColor! {
        didSet {
            headerLayer.backgroundColor = headerColor.cgColor
        }
    }



    var inputs: [ControllableProperty] {
        return representedObject.xinputs
    }

    var outputs: [ControllableProperty] {
        return representedObject.xoutputs
    }

    var connections = [ConnectionView]()

    // Private
    fileprivate var titleLabel = NSTextField(labelWithString: "Node")
    fileprivate var stackView = NSStackView(views: [])

    fileprivate let headerLayer = CALayer()
    fileprivate var lastMousePoint: CGPoint!

    public override var isFlipped: Bool { return true }
    public override var wantsUpdateLayer: Bool { return true }

    fileprivate let kCornerRadius: CGFloat       = 12
    fileprivate let kHeaderHeight: CGFloat       = 24
    fileprivate let kMinNodeWidth: CGFloat       = 100
    fileprivate let kMinNodeHeight: CGFloat      = 100
    fileprivate let kRowHeight: CGFloat          = 24
    fileprivate let kTitleMargin: CGFloat        = 10
    fileprivate let kTitleVerticalInset: CGFloat = 4

    public override func layout() {
        super.layout()
        headerLayer.frame = CGRect(x: 0, y: 0, width: bounds.width, height: kHeaderHeight)
    }

    func setupHeader() {
        headerLayer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        headerLayer.cornerRadius  = kCornerRadius
        layer?.addSublayer(headerLayer)
    }

    fileprivate func setupShadow() {
        let shadow              = NSShadow()
        shadow.shadowOffset     = NSSize(width: 0, height: 0)
        shadow.shadowBlurRadius = 10
        self.shadow             = shadow
    }

    fileprivate func setupOutputs() {
        guard !outputs.isEmpty else { return }
        for output in outputs {
            let row = connectionRowForProperty(output, isInput: false)
            stackView.addArrangedSubview(row)
            constraintHorizontalEdgesOf(row, to: stackView)
        }
        stackView.setCustomSpacing(20, after: stackView.arrangedSubviews.last!)
    }

    fileprivate func setupInputs() {
        for input in inputs {
            let row = connectionRowForProperty(input, isInput: true)
            stackView.addArrangedSubview(row)
            constraintHorizontalEdgesOf(row, to: stackView)
        }
    }

    fileprivate func connectionRowForProperty(_ property: ControllableProperty, isInput: Bool) -> NSView {
        let connection               = ConnectionView()
        connection.isInput           = isInput
        connections.append(connection)

        let label                    = NSTextField(labelWithString: property.name)
        label.font                   = NSFont.systemFont(ofSize: 14)
        label.textColor              = NSColor.textColor
        label.alignment              = isInput ? .left : .right

        let horizontalStack          = NSStackView(views: isInput ? [connection, label] : [label, connection])
        horizontalStack.distribution = .fill
        horizontalStack.spacing      = 8

        let slider = NSSlider(value: 0, minValue: 0, maxValue: 1, target: property, action: #selector(setter: ControllableProperty.value))

        let verticalStack = NSStackView(views: [])
        verticalStack.orientation = .vertical
        verticalStack.alignment = .leading
        verticalStack.distribution = .fill
        verticalStack.spacing = 8

        verticalStack.addArrangedSubview(horizontalStack)
        verticalStack.addArrangedSubview(slider)

        return verticalStack
    }

    fileprivate func constraintHorizontalEdgesOf(_ a: NSView, to b: NSView) {
        a.leadingAnchor.constraint(equalTo: b.leadingAnchor).isActive = true
        a.trailingAnchor.constraint(equalTo: b.trailingAnchor).isActive = true
    }

    public override func updateLayer() {
        super.updateLayer()
        headerColor        = NSColor.textBackgroundColor
        color              = NSColor.underPageBackgroundColor.withAlphaComponent(0.85)
        layer?.borderColor = isSelected ? NSColor.selectedControlColor.cgColor : NSColor.clear.cgColor
    }


}

extension NodeView {

    var isMouseOverConnection: Bool {
        return connections.lazy.first(where: { $0.isHighlighted }) != nil
    }

    public override func becomeFirstResponder() -> Bool {
        isSelected = true
        return true
    }

    public override func resignFirstResponder() -> Bool {
        isSelected = false
        return true
    }

    public override var acceptsFirstResponder: Bool {
        return true
    }

    public override func acceptsFirstMouse(for event: NSEvent?) -> Bool {
        return true
    }

    public override func hitTest(_ point: NSPoint) -> NSView? {
        if isMouseOverConnection {
            return nil
        }
        return super.hitTest(point)
    }

    override public func mouseDown(with event: NSEvent) {
        lastMousePoint = superview!.convert(event.locationInWindow, from: nil)
        window?.makeFirstResponder(self)
    }

    override public func mouseDragged(with event: NSEvent) {
        guard lastMousePoint != nil else {
            return
        }
        let newPoint = superview!.convert(event.locationInWindow, from: nil)
        var origin   = frame.origin
        origin.x += newPoint.x - lastMousePoint.x
        origin.y += newPoint.y - lastMousePoint.y
        setFrameOrigin(origin)
        lastMousePoint = newPoint

        (nextResponder as? NSView)?.needsDisplay = true
    }

    override public func mouseUp(with event: NSEvent) {
        lastMousePoint = nil
    }

}
