//
//  ViewController.swift
//  ElementsPrototype
//
//  Created by Vasilis Akoinoglou on 20/11/2018.
//  Copyright © 2018 Vasilis Akoinoglou. All rights reserved.
//

import Cocoa
import SceneKit
import SceneKit.ModelIO

extension SCNMaterialProperty: ControllableProperty {
    var name: String {
        get {
            return ""
        }
        set {
        }
    }

    var value: CGFloat {
        get {
            return intensity
        }
        set {
            intensity = newValue
        }
    }
}

extension MDLMaterialProperty: ControllableProperty {
    var value: CGFloat {
        get {
            return CGFloat(floatValue)
        }
        set {
            floatValue = Float(newValue)
        }
    }
}

extension MDLMaterialPropertyNode: NodeIO {
    var xinputs: [ControllableProperty] {
        return inputs
    }

    var xoutputs: [ControllableProperty] {
        return outputs
    }
}

class ViewController: NSViewController {

    @IBOutlet var nodeBoardView: NodeBoardView!
    @IBOutlet var sceneView: DraggableSceneView!

    var scene = SCNScene()

    @objc dynamic var material: SCNMaterial!
    @objc dynamic var mdlMaterial: MDLMaterial!
    @objc dynamic var model: SCNNode!
    @objc dynamic var mesh: MDLMesh!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createDefaultEnvironment()
        createDefaultMaterial()
        createDefaultObject()
        createDefaultCamera()
        //createDefaultNode()

        sceneView.scene               = scene
        sceneView.allowsCameraControl = true
    }

    func createDefaultEnvironment() {
        let environment                     = NSImage(named: NSImage.Name("spherical"))
        scene.lightingEnvironment.contents  = environment
        scene.lightingEnvironment.intensity = 2.0

        let background                      = NSImage(named: NSImage.Name("sphericalBlurred"))
        scene.background.contents           = background
    }

    func createDefaultMaterial() {
        mdlMaterial = MDLMaterial(name: "Material", scatteringFunction: MDLPhysicallyPlausibleScatteringFunction())
        material    = SCNMaterial(mdlMaterial: mdlMaterial)
    }

    func createDefaultObject() {
        // let object = MDLAsset(url: Bundle.main.url(forResource: "Rmk3", withExtension: "obj")!)
        // let sphereNode = SCNNode(mdlObject: object.object(at: 0))
        mesh = MDLMesh(scnGeometry: SCNBox(width: 10, height: 10, length: 10, chamferRadius: 0))
        for s in (mesh?.submeshes as? [MDLSubmesh]) ?? [] {
            s.material = mdlMaterial
        }
        model = SCNNode(mdlObject: mesh)
        scene.rootNode.addChildNode(model)
    }

    func createDefaultCamera() {
        let cameraNode              = SCNNode()
        cameraNode.camera           = SCNCamera()
        cameraNode.position         = SCNVector3(x: 0, y: 0, z: 40)
        cameraNode.camera?.wantsHDR = true
        scene.rootNode.addChildNode(cameraNode)
    }

    func createDefaultNode() {

        let inputs: [MDLMaterialProperty] = {
            var t = [MDLMaterialProperty]()
            for i in 0..<mdlMaterial.count {
                t.append(mdlMaterial[i]!)
            }
            return t
        }()

        //let minputs: [SCNMaterialProperty] = [material.diffuse, material.metalness, material.roughness]

        let pnode = MDLMaterialPropertyNode(inputs: inputs, outputs: []) { (node) in

        }
        
        let node = NodeView(object: pnode)
        nodeBoardView.addSubview(node)
    }

    @IBAction func addCube(_ sender: Any?) {
        let node = SCNNode(geometry: SCNBox(width: 10, height: 10, length: 10, chamferRadius: 0))
        scene.rootNode.addChildNode(node)
    }

    override var representedObject: Any? {
        didSet {
            
        }
    }

    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        if let materialController = segue.destinationController as? MaterialController {
            materialController.representedObject = material
        }
    }


}

